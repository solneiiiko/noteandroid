package test.notes.api;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.UUID;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import test.notes.domain.Note;
import test.notes.domain.NoteApiModel;

/**
 * Класс для работы с удаленным хранилищем
 *      Оборачивает объекты в удобный для UC формат
 *  @author Шумилова О.Н.
 */
public class RemoteRepository {
    @NonNull
    private final NotesAPI mNotesAPI;

    public RemoteRepository(NotesAPI api) {
        mNotesAPI = api;
    }

    @NonNull
    public Single<Note> update(@NonNull Note note) {
         return mNotesAPI.updateNote(note.getUUID(), new NoteApiModel(note))
                 .subscribeOn(Schedulers.io())
                 .map(Note::new);
    }

    @NonNull
    public Single<Note> create(@NonNull Note note) {
        return mNotesAPI.createNote(new NoteApiModel(note))
                .subscribeOn(Schedulers.io())
                .map(Note::new);
    }

    @NonNull
    public Single<Note> delete(@NonNull UUID uuid) {
        return mNotesAPI.deleteNote(uuid)
                .subscribeOn(Schedulers.io())
                .map(Note::new);
    }

    @NonNull
    public Single<List<Note>> getList() {
        return mNotesAPI.getNotes()
                .subscribeOn(Schedulers.io())
                .toObservable()
                .flatMapIterable(list -> list)
                .map(Note::new)
                .toList();
    }
}
