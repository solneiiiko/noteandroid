package test.notes.api;


import java.util.List;
import java.util.UUID;

import io.reactivex.Single;
import retrofit2.http.*;
import test.notes.domain.NoteApiModel;

/**
 * Интерфейс для описания АПИ заметок
 * @author Шумилова О.Н.
 */
public interface NotesAPI {
    @GET("/notes")
    Single<List<NoteApiModel>> getNotes();

    @GET("/notes/{uid}")
    Single<NoteApiModel> getNote(
            @Path("uid")UUID uid
    );

    @POST("/notes")
    Single<NoteApiModel> createNote(
        @Body NoteApiModel note
    );

    @PUT("/notes/{uid}")
    Single<NoteApiModel> updateNote(
        @Path("uid")UUID uidPath,
        @Body NoteApiModel note
    );

    @DELETE("/notes/{uid}")
    Single<NoteApiModel> deleteNote(
        @Path("uid")UUID uid
    );
}
