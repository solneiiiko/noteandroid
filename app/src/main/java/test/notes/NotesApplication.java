package test.notes;

import android.app.Application;
import android.support.annotation.NonNull;

import com.facebook.stetho.Stetho;

import test.bases.injection.AppComponent;
import test.bases.injection.ContextModule;
import test.bases.injection.DaggerAppComponent;
import test.notes.injection.NoteComponent;

public class NotesApplication extends Application {
    @SuppressWarnings("NullableProblems")
    @NonNull
    private NoteComponent mNoteComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        AppComponent appComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
        mNoteComponent = appComponent.getNoteComponent();

        initStetho();
    }

    @NonNull
    public NoteComponent getNoteComponent() {
        return mNoteComponent;
    }

    private void initStetho() {
        Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);

        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this)
        );
        Stetho.Initializer initializer = initializerBuilder.build();
        Stetho.initialize(initializer);
    }
}
