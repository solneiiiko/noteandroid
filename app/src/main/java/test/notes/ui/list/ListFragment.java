package test.notes.ui.list;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import test.notes.MainActivity;
import test.notes.NotesApplication;
import test.notes.databinding.FragmentNotesListBinding;
import test.notes.R;
import test.notes.domain.Note;

/**
 * Фрагмент со списком заметок
 * @author Шумилова О.Н.
 */
public class ListFragment extends Fragment {
    @SuppressWarnings("NullableProblems")
    @NonNull
    private ListViewModel mViewModel;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private FloatingActionButton mCreateNoteBtn;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private ListAdapter mListAdapter;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private RecyclerView mRecyclerView;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private TextView mEmptyText;
    @SuppressWarnings("NullableProblems")
    @NonNull
    RecyclerView.LayoutManager mLayoutManager;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private Button mReloadListBtn;
    @Nullable
    private Observer mListObserver;
    @Nullable
    private Observer mErrorObserver;

    @NonNull
    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(
                this,
                new ListViewModel.Factory(
                        ((NotesApplication)requireActivity().getApplication()).getNoteComponent().get())
        ).get(ListViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();

        initActionBar();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentNotesListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notes_list, container, false);

        View view = binding.getRoot();
        mCreateNoteBtn = view.findViewById(R.id.createNodeBtn);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mEmptyText = view.findViewById(R.id.emtyText);
        mReloadListBtn = view.findViewById(R.id.reloadListBtn);

        binding.setListViewModel(mViewModel);

        initList();
        addObservers();
        addListeners();

        return view;
    }

    private void initActionBar() {
        ActionBar actionBar = ((AppCompatActivity)requireActivity()).getSupportActionBar();

        if (actionBar ==  null) {
            return;
        }

        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(R.string.app_name);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mCreateNoteBtn.setOnClickListener(null);
        mListAdapter.setOnItemClickListener(null);

        removeObservers();
    }

    private void initList() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mLayoutManager = new LinearLayoutManager(requireActivity());
        } else {
            mLayoutManager = new GridLayoutManager(requireActivity(), 2);
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        mListAdapter = new ListAdapter();
        mRecyclerView.setAdapter(mListAdapter);
    }

    private void addObservers() {
        mListObserver = new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                if (notes.size() > 0) {
                    mEmptyText.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    mEmptyText.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }

                mListAdapter.setItems(notes);
            }
        };

        mErrorObserver = new Observer<Error>() {
            @Override
            public void onChanged(@Nullable Error error) {
                if (error != null) {
                    showErrorMessage(error);
                }
            }
        };

        mViewModel.getList().observeForever(mListObserver);

        mViewModel.getError().observeForever(mErrorObserver);
    }

    private void removeObservers() {
        mViewModel.getList().removeObserver(mListObserver);
        mViewModel.getError().removeObserver(mErrorObserver);

        mListObserver = null;
        mErrorObserver = null;
    }

    private void addListeners() {
        mCreateNoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)requireActivity()).showNewNoteFragment();
            }
        });

        mListAdapter.setOnItemClickListener(new ListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull Note note) {
                ((MainActivity)requireActivity()).showEditNoteFragment(note.getUUID());
            }

            @Override
            public void onDeleteItemClick(@NonNull Note note) {
                mViewModel.deleteNote(note.getUUID());
            }
        });

        mReloadListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.reloadList();
            }
        });
    }

    private void showErrorMessage(@NonNull Error error) {
        Toast.makeText(requireActivity(), error.getMessage(), Toast.LENGTH_LONG)
                .show();
    }
}
