package test.notes.ui.list;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import test.notes.databinding.FragmentFilterBinding;
import test.notes.domain.Note;
import test.notes.R;

/**
 * Фрагмент с фильтром и сортировкой
 * @author Шумилова О.Н.
 */
public class FilterFragment extends Fragment {
    @NonNull
    private static String DATE_FORMAT_KEY = "dateTextFormat";
    @NonNull
    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
    @SuppressWarnings("NullableProblems")
    @NonNull
    private View mView;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private ImageButton mCreatedAtUpBtn;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private ImageButton mCreatedAtDownBtn;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private ImageButton mSortTitleUpBtn;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private ImageButton mSortTitleDownBtn;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private TextView mDateTextView;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private ListViewModel mViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getParentFragment() == null) {
            return null;
        }

        FragmentFilterBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false);

        mView = binding.getRoot();

        mViewModel = ViewModelProviders.of(getParentFragment()).get(ListViewModel.class);

        binding.setViewModel(mViewModel);
        mCreatedAtUpBtn = binding.sortCreatedAtUp;
        mCreatedAtDownBtn = binding.sortCreatedAtDown;
        mSortTitleUpBtn = binding.sortTitleUp;
        mSortTitleDownBtn = binding.sortTitleDown;
        mDateTextView = binding.createdAtFromText;

        addListeners();

        return mView;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mDateTextView.getText() != null) {
            outState.putString(DATE_FORMAT_KEY, mDateTextView.getText().toString());
        }
    }

    public void addListeners() {
        mView.findViewById(R.id.createdAtFrom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(requireActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        calendar.set(Calendar.YEAR, i);
                        calendar.set(Calendar.MONTH, i1);
                        calendar.set(Calendar.DAY_OF_MONTH, i2);

                        Date date = calendar.getTime();
                        mDateTextView.setText(DATE_FORMAT.format(date));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });

        mCreatedAtUpBtn.setOnClickListener(getOnClickSortBtn(Note.DestroyDateUp, mCreatedAtUpBtn));
        mCreatedAtDownBtn.setOnClickListener(getOnClickSortBtn(Note.DestroyDateDown, mCreatedAtDownBtn));

        mSortTitleUpBtn.setOnClickListener(getOnClickSortBtn(Note.TitleUp, mSortTitleUpBtn));
        mSortTitleDownBtn.setOnClickListener(getOnClickSortBtn(Note.TitleDown, mSortTitleDownBtn));
    }

    private View.OnClickListener getOnClickSortBtn(Comparator<Note> comparator, ImageButton button) {
        return new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                mViewModel.setSort(comparator);
                // TODO посмотреть биндинг
                mCreatedAtDownBtn.setColorFilter(getResources().getColor(R.color.colorFont));
                mCreatedAtUpBtn.setColorFilter(getResources().getColor(R.color.colorFont));
                mSortTitleDownBtn.setColorFilter(getResources().getColor(R.color.colorFont));
                mSortTitleUpBtn.setColorFilter(getResources().getColor(R.color.colorFont));

                button.setColorFilter(getResources().getColor(R.color.colorGray));
            }
        };
    }
}
