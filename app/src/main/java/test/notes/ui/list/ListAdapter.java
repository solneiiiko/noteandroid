package test.notes.ui.list;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import test.notes.databinding.MiniNoteItemBinding;
import test.notes.domain.Note;

/**
 * Адаптер для списка заметок
 * @author Шумилова О.Н.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.NoteHolder> {
    @Nullable
    private OnItemClickListener mOnItemClickListener;
    @NonNull
    private List<Note> mItems = new ArrayList<>();

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflate = LayoutInflater.from(viewGroup.getContext());

        MiniNoteItemBinding binding = MiniNoteItemBinding.inflate(inflate, viewGroup, false);
        return new NoteHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder noteHolder, int i) {
        noteHolder.setNote(mItems.get(i));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setOnItemClickListener(@Nullable OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setItems(@NonNull List<Note> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    @NonNull
    public Note getItem(int position) {
        return mItems.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(@NonNull Note note);

        void onDeleteItemClick(@NonNull Note note);
    }

    class NoteHolder extends RecyclerView.ViewHolder implements OnItemClickListener {
        @NonNull
        private MiniNoteItemBinding mBinding;
        @Nullable
        private Note mNote;

        NoteHolder(@NonNull View itemView) {
            super(itemView);

            mBinding = DataBindingUtil.bind(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mNote != null) {
                        onItemClick(mNote);
                    }
                }
            });

            mBinding.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mNote != null) {
                        onDeleteItemClick(mNote);
                    }
                }
            });
        }

        void setNote(@NonNull Note note) {
            mNote = note;
            mBinding.setNote(note);
        }

        @Override
        public void onItemClick(@NonNull Note note) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(note);
            }
        }

        @Override
        public void onDeleteItemClick(@NonNull Note note) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onDeleteItemClick(note);
            }
        }
    }
}
