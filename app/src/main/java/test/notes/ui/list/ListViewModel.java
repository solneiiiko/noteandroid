package test.notes.ui.list;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.reactivex.android.schedulers.AndroidSchedulers;
import test.bases.ui.BaseViewModel;
import test.notes.datamanager.NotesDataManager;
import test.notes.domain.Note;

/**
 * ViewModel для списка заметок
 *      изменяет свое состояние, на которое подписывается view (фрагмент)
 * @author Шумилова О.Н.
 */
public class ListViewModel extends BaseViewModel {
    @NonNull
    private static final String LOG_TAG = "ListViewModel";
    @NonNull
    private final static Comparator<Note> DEFAULT_SORT = Note.TitleUp;
    @NonNull
    public final ObservableBoolean isDataLoading = new ObservableBoolean();
    @NonNull
    public final ObservableBoolean isLoadingError = new ObservableBoolean();
    @NonNull
    private final MutableLiveData<Error> mError = new MutableLiveData<>();
    @NonNull
    private MutableLiveData<List<Note>> mList = new MutableLiveData<>();
    @NonNull
    private MutableLiveData<String> mFilterText = new MutableLiveData<>();
    @NonNull
    private MutableLiveData<Date> mFilterDestroyDate = new MutableLiveData<>();
    @NonNull
    private MutableLiveData<Comparator<Note>> mSort = new MutableLiveData<>();
    @NonNull
    private NotesDataManager mNotesDataManager;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private android.arch.lifecycle.Observer<String> mFilterTextObserver;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private android.arch.lifecycle.Observer<Date> mFilterDateObserver;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private android.arch.lifecycle.Observer<Comparator<Note>> mSortObserver;

    ListViewModel(@NonNull NotesDataManager notesDataManager) {
        mSort.setValue(DEFAULT_SORT);

        mNotesDataManager = notesDataManager;

        if (mNotesDataManager.getListSynchronizedAt() == null) {
            reloadList();
        }

        addListeners();
    }


    private void addListeners() {
        mFilterTextObserver = filterText -> {
            if (mNotesDataManager.getListSynchronizedAt() != null) {
                setList(mList.getValue(), true);
            }
        };

        mFilterDateObserver = date -> {
            if (mNotesDataManager.getListSynchronizedAt() != null) {
                setList(mList.getValue(), true);
            }
        };

        mSortObserver = noteComparator -> {
            if (mNotesDataManager.getListSynchronizedAt() != null) {
                setList(mList.getValue(), false);
            }
        };

        mFilterText.observeForever(mFilterTextObserver);
        mFilterDestroyDate.observeForever(mFilterDateObserver);
        mSort.observeForever(mSortObserver);
    }

    public String getFilterText() {
        return mFilterText.getValue();
    }

    public void setFilterText(String s) {
        mFilterText.setValue(s);
    }

    public Date getFilterDestroyDate() {
        return mFilterDestroyDate.getValue();
    }

    public void setFilterDestroyDate(Date d) {
        mFilterDestroyDate.setValue(d);
    }

    public void setSort(Comparator<Note> comparator) {
        mSort.setValue(comparator);
    }

    public LiveData<Comparator<Note>> getSort() {
        return mSort;
    }

    public LiveData<Error> getError() {
        return mError;
    }

    @NonNull
    public LiveData<List<Note>> getList() {
        return mList;
    }


    private void setList(@Nullable List<Note> list, boolean isFiltered) {
        if (list != null) {
            if (isFiltered) {
                list = getFilterList(list);
            }

            mList.postValue(getSortList(list));
            isDataLoading.set(false);
        }
    }

    @NonNull
    private List<Note> getFilterList(@NonNull List<Note> list) {
        List<Note> resultList = new ArrayList<>();

        for (Note note : list) {
            boolean deletedAt = mFilterDestroyDate.getValue() == null || note.getDestroyDate() == null
                    || note.getDestroyDate().compareTo(mFilterDestroyDate.getValue()) >= 0;
            boolean textFilter = mFilterText.getValue() == null || note.getTitle().toLowerCase().contains(mFilterText.getValue().toLowerCase())
                    || note.getContent().contains(mFilterText.getValue());
            if (deletedAt && textFilter) {
                resultList.add(note);
            }
        }

        return resultList;
    }

    @NonNull
    private List<Note> getSortList(@NonNull List<Note> list) {
        Collections.sort(list, mSort.getValue());
        return list;
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        mFilterText.removeObserver(mFilterTextObserver);
        mFilterDestroyDate.removeObserver(mFilterDateObserver);
        mSort.removeObserver(mSortObserver);
    }

    public void deleteNote(UUID noteId) {
        mNotesDataManager.delete(noteId);
    }

    public void reloadList() {
        isDataLoading.set(true);
        isLoadingError.set(false);

        mDisposables.add(
                mNotesDataManager.getListFromServer()
                        .subscribe(notes -> {
                            mError.postValue(null);
                            isLoadingError.set(false);
                            subscribeLocalList();
                        }, e -> {
                            isLoadingError.set(true);
                            isDataLoading.set(false);
                            mError.setValue(new Error(e.getMessage()));
                        }));
    }

    private void subscribeLocalList() {
        mDisposables.add(mNotesDataManager.getListFromLocal()
            .subscribe(notes -> {
                setList(notes, true);
            }, e -> {
                Log.e(LOG_TAG, "subscribeLocalList: ", e);
            }));
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @NonNull
        private final NotesDataManager mNotesDataManager;

        Factory(@NonNull NotesDataManager notesDataManager) {
            super();
            mNotesDataManager = notesDataManager;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if (modelClass == ListViewModel.class) {
                return (T) new ListViewModel(mNotesDataManager);
            }

            throw new ClassCastException();
        }
    }
}
