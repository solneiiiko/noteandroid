package test.notes.ui.edit;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.reactivex.schedulers.Schedulers;
import test.bases.ui.BaseViewModel;
import test.notes.datamanager.NotesDataManager;
import test.notes.domain.Note;

public class EditPagerViewModel extends BaseViewModel {
    @NonNull
    private static final String LOG_TAG = "EditPagerViewModel";
    @NonNull
    public final ObservableBoolean isDataLoading = new ObservableBoolean();
    @NonNull
    private NotesDataManager mNotesDataManager;
    @NonNull
    private MutableLiveData<List<Note>> mList = new MutableLiveData<>();
    @Nullable
    private final UUID mNoteId;

    EditPagerViewModel(@Nullable UUID noteId, @NonNull NotesDataManager notesDataManager) {
        mNoteId = noteId;

        mNotesDataManager = notesDataManager;
        isDataLoading.set(true);

        initList();
    }

    private void initList() {
        isDataLoading.set(true);
        if (mNoteId != null) {
            mDisposables.add(mNotesDataManager.getListFromLocal()
                .subscribeOn(Schedulers.computation())
                .subscribe(
                    notes -> {mList.postValue(notes);},
                    e -> {
                        Log.e(LOG_TAG, "initList: ", e);
                    }));
        } else {
            mList.setValue(Arrays.asList(new Note()));
        }
        isDataLoading.set(false);
    }

    @NonNull
    public LiveData<List<Note>> getList() {
        return mList;
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @Nullable
        private final UUID mNoteId;
        @NonNull
        private final NotesDataManager mNotesDataManager;

        Factory(@Nullable UUID noteId, NotesDataManager notesDataManager) {
            super();
            mNoteId = noteId;
            mNotesDataManager = notesDataManager;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if (modelClass == EditPagerViewModel.class) {
                return (T) new EditPagerViewModel(mNoteId, mNotesDataManager);
            }

            throw new ClassCastException(modelClass.toString() + "is not equals to EditPagerViewModel");
        }
    }
}
