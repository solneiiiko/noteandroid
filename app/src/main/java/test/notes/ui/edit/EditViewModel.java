package test.notes.ui.edit;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.UUID;

import io.reactivex.observers.DisposableSingleObserver;
import test.bases.ui.BaseViewModel;
import test.notes.datamanager.NotesDataManager;
import test.notes.domain.Note;

public class EditViewModel extends BaseViewModel {
    @NonNull
    private static final String LOG_TAG = "EditViewModel";
    @Nullable
    private final UUID mNoteUUID;
    @NonNull
    public NotesDataManager mNotesDataManager;
    @NonNull
    private ObservableField<Note> mNote = new ObservableField<>();

    EditViewModel(@Nullable UUID noteUUID, NotesDataManager notesDataManager) {
        mNotesDataManager = notesDataManager;

        mNoteUUID = noteUUID;
        initNote();
    }

    public void saveNote() {
        Note note = mNote.get();
        if ( note != null) {
            mNotesDataManager.save(note);
        }
    }

    public void deleteNote() {
        if (mNoteUUID != null) {
            mNotesDataManager.delete(mNoteUUID);
        }
    }

    @NonNull
    public ObservableField<Note> getNote() {
        return mNote;
    }

    public void setNote(@NonNull ObservableField<Note> note) {
        mNote.set(note.get());
    }

    private void initNote() {
        if (mNoteUUID == null) {
            mNote.set(new Note());
            return;
        }

        mNotesDataManager.getById(mNoteUUID)
            .subscribe(new DisposableSingleObserver<Note>() {
                @Override
                public void onSuccess(Note note) {
                    try {
                        mNote.set(note.clone());
                    } catch (CloneNotSupportedException ex) {
                        Log.e(LOG_TAG, "initNote: ", ex);
                        // todo показать ошибку
                    }
                }

                @Override
                public void onError(Throwable e) {
                    Log.e(LOG_TAG, "initNote: ", e);
                    // todo показать ошибку
                }
            });
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @Nullable
        private final UUID mNoteId;
        @NonNull
        private final NotesDataManager mNotesDataManager;

        Factory(@Nullable UUID noteId, @NonNull NotesDataManager notesDataManager) {
            super();
            mNoteId = noteId;
            mNotesDataManager = notesDataManager;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if (modelClass == EditViewModel.class) {
                return (T) new EditViewModel(mNoteId, mNotesDataManager);
            }

            throw  new ClassCastException();
        }
    }
}
