package test.notes.ui.edit;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.util.UUID;

import test.notes.NotesApplication;
import test.notes.databinding.FragmentEditNoteBinding;
import test.notes.R;

public class EditFragment extends Fragment {
    private static final String LOG_TAG = "EditFragment";
    private static final String FIELD_NOTE_ID = "noteId";
    @SuppressWarnings("NullableProblems")
    @NonNull
    private EditViewModel mViewModel;

    @NonNull
    public static EditFragment newInstance(UUID noteUUID) {
        Bundle args = new Bundle();
        args.putString(FIELD_NOTE_ID, noteUUID.toString());

        EditFragment fragment = new EditFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    public static EditFragment newInstance() {
        return new EditFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final UUID noteUUID;

        Bundle args = getArguments();

        noteUUID = args == null ? null : UUID.fromString(args.getString(FIELD_NOTE_ID));

        mViewModel = ViewModelProviders.of(this, new EditViewModel.Factory(
                noteUUID,
                ((NotesApplication)requireActivity().getApplication()).getNoteComponent().get())).get(EditViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentEditNoteBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_note, container, false);

        binding.setViewModel(mViewModel);

        return binding.getRoot();
    }

    @Override
    public void onStop() {
        super.onStop();

        hideKeyboard();
    }

    public void saveNote() {
        mViewModel.saveNote();
    }

    public void deleteNote() {
        mViewModel.deleteNote();
    }

    private void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager)requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(requireActivity().getWindow().getDecorView().getWindowToken(), 0);
        } catch (NullPointerException ex) {
            Log.e(LOG_TAG, "hideKeyboard", ex);
        }
    }
}
