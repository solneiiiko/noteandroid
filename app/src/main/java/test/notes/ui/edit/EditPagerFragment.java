package test.notes.ui.edit;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import test.notes.MainActivity;
import test.notes.NotesApplication;
import test.notes.databinding.EditNotePagerBinding;
import test.notes.domain.Note;
import test.notes.R;

public class EditPagerFragment extends Fragment {
    private static final String FIELD_NOTE_ID = "noteId";
    @Nullable
    private UUID mUUID;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private View mView;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private ViewPager mViewPager;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private PagerAdapter mPagerAdapter;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private EditPagerViewModel mViewModel;
    @NonNull
    private Observer<List<Note>> mListObserver;

    @NonNull
    public static EditPagerFragment newInstance(UUID uuid) {
        Bundle args = new Bundle();
        args.putString(FIELD_NOTE_ID, uuid.toString());

        EditPagerFragment fragment = new EditPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    public static EditPagerFragment newInstance() {
        return new EditPagerFragment();
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        EditNotePagerBinding binding = DataBindingUtil.inflate(inflater, R.layout.edit_note_pager, container, false);
        Bundle args = getArguments();

        if (args != null) {
            mUUID = UUID.fromString(args.getString(FIELD_NOTE_ID));
        }

        mView = binding.getRoot();
        mViewModel = ViewModelProviders.of(this, new EditPagerViewModel.Factory(
                mUUID,
                ((NotesApplication)requireActivity().getApplication()).getNoteComponent().get())).get(EditPagerViewModel.class);

        binding.setViewModel(mViewModel);

        initPageView();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();

        initActionBar();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mViewModel.getList().removeObserver(mListObserver);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        menu.findItem(R.id.menu_delete).setVisible(mUUID != null);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        EditFragment editFragment = mPagerAdapter.getRegistaredFragment(mViewPager.getCurrentItem());

        switch (item.getItemId()) {
            case R.id.menu_add:
                editFragment.saveNote();
                closeFragment();
                return true;
            case R.id.menu_delete:
                editFragment.deleteNote();
                closeFragment();
                return true;
            case android.R.id.home:
                closeFragment();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void closeFragment() {
        ((MainActivity)requireActivity()).removeFragment();
    }

    private void initPageView() {
        mPagerAdapter = new PagerAdapter(getChildFragmentManager());

        mViewPager = mView.findViewById(R.id.pagerView);
        mViewPager.setAdapter(mPagerAdapter);

        mListObserver = new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                if (notes != null) {
                    mPagerAdapter.setItems(notes);
                    if (mUUID != null) {
                        mViewPager.setCurrentItem(mPagerAdapter.getPositionByNoteUUID(mUUID));
                    }
                    mViewPager.setVisibility(View.VISIBLE);
                }
            }
        };

        mViewModel.getList().observeForever(mListObserver);
    }

    private void initActionBar() {
        ActionBar actionBar = ((AppCompatActivity)requireActivity()).getSupportActionBar();

        if (actionBar ==  null) {
            return;
        }

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(null);
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        @NonNull
        private SparseArray<EditFragment> registeredFragments = new SparseArray<>();
        @NonNull
        private List<Note> mNoteList = new ArrayList<>();

        PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            UUID noteId = mNoteList.get(i).getUUID();
            if (noteId == null) {
                return EditFragment.newInstance();
            }

            return EditFragment.newInstance(noteId);
        }

        @Override
        public int getCount() {
            return mNoteList.size();
        }

        @NonNull
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            EditFragment fragment = (EditFragment)super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        @NonNull
        EditFragment getRegistaredFragment(int position) {
            return registeredFragments.get(position);
        }

        void setItems(@NonNull List<Note> noteList) {
            mNoteList = noteList;
            notifyDataSetChanged();
        }

        int getPositionByNoteUUID(@NonNull UUID uuid) {
            for (int i = 0; i < mNoteList.size(); i++) {
                if (mNoteList.get(i).getUUID().equals(uuid)) {
                    return i;
                }
            }
            return -1;
        }
    }
}
