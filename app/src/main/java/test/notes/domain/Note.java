package test.notes.domain;

import java.util.Comparator;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.UUID;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Класс, описывающий заметку
 * @author Шумилова О.Н.
 */
public class Note implements Cloneable {
    @NonNull
    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.ENGLISH);
    @NonNull
    public static Comparator<Note> DestroyDateUp = new Comparator<Note>() {
        @Override
        public int compare(Note n1, Note n2) {
            return 0;//n1.getFilterDestroyDate().compareTo(n2.getFilterDestroyDate());
        }
    };
    @NonNull
    public static Comparator<Note> DestroyDateDown = new Comparator<Note>() {
        @Override
        public int compare(Note n1, Note n2) {
            return 0;//n2.getFilterDestroyDate().compareTo(n1.getFilterDestroyDate());
        }
    };
    @NonNull
    public static Comparator<Note> TitleUp = new Comparator<Note>() {
        @Override
        public int compare(Note n1, Note n2) {
            return n1.getTitle().compareToIgnoreCase(n2.getTitle());
        }
    };

    @NonNull
    public static Comparator<Note> TitleDown = new Comparator<Note>() {
        @Override
        public int compare(Note n1, Note n2) {
            return n2.getTitle().compareToIgnoreCase(n1.getTitle());
        }
    };

    @Nullable
    private UUID mUUID;
    @Nullable
    private String mTitle;
    @Nullable
    private String mContent;
    private int mColor;
    @Nullable
    private Date mDestroyDate;
    private int destroy;
    private boolean mIsSynch = false;
    private boolean mIsArchive = false;
    @Nullable
    private Date mSynchAt;

    public Note() {
        mColor = Color.RED;
    }

    public Note(@NonNull NoteApiModel noteApiModel) {
        mUUID = UUID.fromString(noteApiModel.getUUID());
        mColor = Color.parseColor(noteApiModel.getColor());
        mTitle = noteApiModel.getTitle();
        mContent = noteApiModel.getContent();
        mDestroyDate = noteApiModel.getDestroyDate() == null ? null : new Date(noteApiModel.getDestroyDate());
    }

    public Note(@NonNull NoteDaoModel noteDaoModel) {
        mUUID = UUID.fromString(noteDaoModel.getUuid());
        mColor = Color.parseColor(noteDaoModel.getColor());
        mContent = noteDaoModel.getContent();
        mTitle = noteDaoModel.getTitle();
        mSynchAt = new Date(noteDaoModel.getSynchAt());
        mIsSynch = noteDaoModel.getIsSynch() != 0;
        mDestroyDate = noteDaoModel.getDestroyDate() == 0 ? null : new Date(noteDaoModel.getDestroyDate());
        mIsArchive = noteDaoModel.getIsArchive() != 0;
    }

    public UUID getUUID() {
        return mUUID;
    }

    public void generateUUID() {
        mUUID = UUID.randomUUID();
    }

    public void setUUID(@NonNull UUID uuid) {
        mUUID = uuid;
    }

    public int getDestroy() {
        return destroy;
    }

    public void setDestroy(int destroy) {
        this.destroy = destroy;
    }

    public boolean isArchive() {
        return mIsArchive;
    }

    public void setArchive(boolean archive) {
        mIsArchive = archive;
    }

    @NonNull
    public String getTitle() {
        return mTitle == null ? "" : mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    @NonNull
    public String getContent() {
        return mContent == null ? "" : mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public int getColor() {
        return mColor;
    }

    public String getColorRGB() {
        return String.format("#%06X", (0xFFFFFF & mColor));
    }

    public void setColor(String color) {
        mColor = Color.parseColor(color);
    }

    public void setColor(int color) {
        mColor = color;
    }

    @Nullable
    public Date getDestroyDate() {
        return mDestroyDate;
    }

    public void setDestroyDate(@Nullable Date destroyDate) {
        mDestroyDate = destroyDate;
    }

    public String getDestroyDateFormat() {
        if (mDestroyDate == null) {
            return null;
        }

        return DATE_FORMAT.format(mDestroyDate);
    }

    public boolean isSynch() {
        return mIsSynch;
    }

    @Nullable
    public Date getSynchAt() {
        return mSynchAt;
    }

    public void setSynchAt(@Nullable Date synchAt) {
        mSynchAt = synchAt;
    }

    public void setSynch(boolean synch) {
        mIsSynch = synch;
    }

    public Note clone() throws CloneNotSupportedException {
        Note note = (Note)super.clone();

        note.setTitle(mTitle);
        note.setContent(mContent);
        note.setSynch(mIsSynch);
        note.setUUID(mUUID);
        note.setColor(mColor);
        note.setDestroyDate(mDestroyDate);

        return note;
    }
}
