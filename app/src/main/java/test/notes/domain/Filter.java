package test.notes.domain;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;

public class Filter {
    @Nullable
    private Date mDestroyDate;
    @Nullable
    private String mText;
    private Boolean mIsArchive = false;

    @Nullable
    public Date getDestroyDate() {
        return mDestroyDate;
    }

    public void setDestroyDate(@Nullable Date destroyDate) {
        mDestroyDate = destroyDate;
    }

    @Nullable
    public String getText() {
        return mText;
    }

    public void setText(@Nullable String text) {
        mText = text;
    }

    public Boolean getArchive() {
        return mIsArchive;
    }

    public void setArchive(Boolean archive) {
        mIsArchive = archive;
    }
}
