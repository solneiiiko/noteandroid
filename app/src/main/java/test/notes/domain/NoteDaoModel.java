package test.notes.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

@Entity(tableName = "notes")
public class NoteDaoModel {
    @PrimaryKey
    @ColumnInfo(name = "uuid")
    @NonNull
    private String mUuid;
    @ColumnInfo(name = "destroy_date")
    @Nullable
    private Long mDestroyDate;
    @ColumnInfo(name = "is_synch")
    private int mIsSynch;
    @ColumnInfo(name = "synch_at")
    private long mSynchAt;
    @ColumnInfo(name = "is_archive")
    private int mIsArchive;
    @ColumnInfo(name = "title")
    @Nullable
    private String mTitle;
    @ColumnInfo(name = "content")
    @Nullable
    private String mContent;
    @ColumnInfo(name = "color")
    @Nullable
    private String mColor;

    public NoteDaoModel() {

    }

    public NoteDaoModel(Note note) {
        if (note.getUUID() == null) {
            throw new Error(); // todo
        }

        mUuid = note.getUUID().toString();

        mTitle = note.getTitle();
        mContent = note.getContent();
        mColor = note.getColorRGB();
        mDestroyDate = note.getDestroyDate() == null ? 0 : note.getDestroyDate().getTime();
        mSynchAt = note.getSynchAt() == null ? 0 : note.getSynchAt().getTime();
        mIsSynch = note.isSynch() ? 1 : 0;
        mIsArchive = note.isArchive() ? 1 : 0;
    }

    @NonNull
    public String getUuid() {
        return mUuid;
    }

    public void setUuid(@NonNull String uuid) {
        mUuid = uuid;
    }

    @Nullable
    public Long getDestroyDate() {
        return mDestroyDate;
    }

    public void setDestroyDate(@Nullable Long destroyDate) {
        mDestroyDate = destroyDate;
    }

    public int getIsSynch() {
        return mIsSynch;
    }

    public void setIsSynch(int isSynch) {
        mIsSynch = isSynch;
    }

    public long getSynchAt() {
        return mSynchAt;
    }

    public void setSynchAt(long synchAt) {
        mSynchAt = synchAt;
    }

    public int getIsArchive() {
        return mIsArchive;
    }

    public void setIsArchive(int isArchive) {
        mIsArchive = isArchive;
    }

    @Nullable
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(@Nullable String title) {
        mTitle = title;
    }

    @Nullable
    public String getContent() {
        return mContent;
    }

    public void setContent(@Nullable String content) {
        mContent = content;
    }

    @Nullable
    public String getColor() {
        return mColor;
    }

    public void setColor(@Nullable String color) {
        mColor = color;
    }
}
