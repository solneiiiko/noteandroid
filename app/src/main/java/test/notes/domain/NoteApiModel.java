package test.notes.domain;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class NoteApiModel {
    @SerializedName("uid")
    @NonNull
    private String mUUID;
    @SerializedName("title")
    @Nullable
    private String mTitle;
    @SerializedName("content")
    @Nullable
    private String mContent;
    @SerializedName("color")
    @Nullable
    private String mColor = "#FF0000";
    @SerializedName("destroy_date")
    @Nullable
    private Long mDestroyDate;

    public NoteApiModel(Note note) {
        if (note.getUUID() != null) {
            mUUID = note.getUUID().toString();
        }
        mTitle = note.getTitle();
        mContent = note.getContent();
        mColor = note.getColorRGB();
        if (note.getDestroyDate() != null) {
            mDestroyDate = note.getDestroyDate().getTime();
        }
    }

    @NonNull
    public String getUUID() {
        return mUUID;
    }

    @Nullable
    public String getTitle() {
        return mTitle;
    }

    @Nullable
    public String getContent() {
        return mContent;
    }

    @Nullable
    public String getColor() {
        return mColor;
    }

    @Nullable
    public Long getDestroyDate() {
        return mDestroyDate;
    }
}
