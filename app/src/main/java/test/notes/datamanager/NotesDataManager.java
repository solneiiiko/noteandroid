package test.notes.datamanager;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import dagger.Module;
import io.reactivex.Flowable;
import io.reactivex.Single;
import test.notes.api.RemoteRepository;
import test.notes.dao.LocalRepository;
import test.notes.domain.Note;

/**
 *  Класс для работы с локальными данными и апи
 *      Выполняет синхронизацию локальных данных с сервером
 *  @author Шумилова О.Н.
 */
public class NotesDataManager {
    private static final String LOG_TAG = "NotesDataManager";
    @NonNull
    private RemoteRepository mRemoteRepository;
    @NonNull
    private LocalRepository mLocalRepository;
    @Nullable
    private Date mListSynchronizedAt;

    public NotesDataManager(@NonNull RemoteRepository remoteRepository, @NonNull LocalRepository localRepository) {
        mRemoteRepository = remoteRepository;
        mLocalRepository = localRepository;
    }

    @NonNull
    public Single<Note> getById(@NonNull UUID uuid) {
        return mLocalRepository.getById(uuid);
    }

    @NonNull
    public Flowable<List<Note>> getListFromLocal() {
        return mLocalRepository.getList();
    }

    @SuppressLint("CheckResult")
    public void save(@NonNull Note note) {
        boolean isNew = note.getUUID() == null;
        beforeSave(note);
        note.setSynch(false);
        mLocalRepository.create(note)
            .subscribe(() -> {
                    }, e -> {
                        Log.e(LOG_TAG, "save: ", e);
                    });
        if (isNew) {
            createOnServer(note);
        } else {
            updateOnServer(note);
        }
    }

    @SuppressLint("CheckResult")
    private void updateOnServer(@NonNull Note note) {
        mRemoteRepository
                .update(note)
                .subscribe(
                        this::synchLocalNote, e -> {
                        Log.e(LOG_TAG, "updateOnServer: ", e);
                    });
    }

    @SuppressLint("CheckResult")
    private void createOnServer(@NonNull Note note) {
        mRemoteRepository
                .create(note)
                .subscribe(this::synchLocalNote, e -> {
                    Log.e(LOG_TAG, "createOnServer: ", e);
                });
    }

    public void delete(@NonNull UUID uuid) {
        deleteOnServer(uuid);
        deleteOnLocal(uuid);
    }

    @SuppressLint("CheckResult")
    private void synchLocalNote(@NonNull Note note) {
        note.setSynch(true);
        mLocalRepository
                .create(note)
                .subscribe(() -> {
                    Log.d(LOG_TAG, "synch");
                }, e -> {
                    Log.e(LOG_TAG, "synchLocalNote: ", e);
                });
    }

    @SuppressLint("CheckResult")
    private void deleteOnServer(@NonNull UUID uuid) {
        mRemoteRepository
                .delete(uuid)
                .subscribe(note -> {
                    Log.d(LOG_TAG, "deleteOnServer");
                }, e -> {
                    Log.e(LOG_TAG, "deleteOnServer: ", e);
                });
    }

    @SuppressLint("CheckResult")
    private void deleteOnLocal(@NonNull UUID uuid) {
        mLocalRepository
                .delete(uuid)
                .subscribe(() -> {
                    Log.d(LOG_TAG, "deleteOnLocal");
                }, e -> {
                    Log.e(LOG_TAG, "deleteOnLocal: ", e);
                });
    }

    private void beforeSave(@NonNull Note note) {
        if (note.getUUID() == null) {
            note.generateUUID();
        }
    }

    @SuppressLint("CheckResult")
    private void saveOnLocal(@NonNull List<Note> notes) {
        for (Note note : notes) {
            if (note.getUUID() != null) {
                mLocalRepository
                        .create(note)
                        .subscribe(() -> {
                            Log.d(LOG_TAG, "saveOnLocal");
                        }, e -> {
                            Log.e(LOG_TAG, "saveOnLocal: ", e);
                        });
            }
        }
    }

    @SuppressLint("CheckResult")
    @NonNull
    public Single<List<Note>> getListFromServer() {
        Single<List<Note>> resSingle = mRemoteRepository.getList().cache();

        resSingle.subscribe(list -> {
                    mListSynchronizedAt = new Date();
                    List<Note> resList = new ArrayList<>(list.size());
                    for (Note note : list) {
                        note.setSynch(true);
                        resList.add(note);
                    }

                    mLocalRepository.create(resList)
                          .subscribe(() -> {
                                Log.d(LOG_TAG, "synch");
                            }, e -> {
                                Log.e(LOG_TAG, "getListFromServer: ", e);
                            });
                }, e -> {
                    Log.e(LOG_TAG, "getListFromServer: ", e);
                });

        return resSingle;
    }

    @Nullable
    public Date getListSynchronizedAt() {
        return mListSynchronizedAt;
    }
}
