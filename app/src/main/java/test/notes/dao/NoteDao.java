package test.notes.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import test.notes.domain.NoteDaoModel;

@Dao
public interface NoteDao {
    @Query("SELECT * FROM notes WHERE is_archive = 0")
    Flowable<List<NoteDaoModel>> getNotes();

    @Query("SELECT * FROM notes WHERE is_archive = 0 AND uuid = :uuid")
    Single<NoteDaoModel> getById(String uuid);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void create(NoteDaoModel noteDaoModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void create(List<NoteDaoModel> noteDaoModels);

    @Query("DELETE FROM notes WHERE uuid = :uuid")
    void delete(String uuid);
}
