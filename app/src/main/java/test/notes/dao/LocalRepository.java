package test.notes.dao;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import test.notes.domain.Note;
import test.notes.domain.NoteDaoModel;

public class LocalRepository {
    @NonNull
    private final NoteDao mNoteDao;

    public LocalRepository(@NonNull NoteDao noteDao) {
        mNoteDao = noteDao;
    }

    @NonNull
    public Single<Note> getById(@NonNull UUID uuid) {
        return mNoteDao.getById(uuid.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map(Note::new);
    }

    @NonNull
    public Flowable<List<Note>> getList() {
        return mNoteDao.getNotes()
                .observeOn(Schedulers.computation())
                .map((noteDaoModels) -> {
                    List<Note> notes = new ArrayList<>(noteDaoModels.size());
                    for (NoteDaoModel noteDaoModel : noteDaoModels) {
                        notes.add(new Note(noteDaoModel));
                    }

                    return notes;
                });
    }

    @NonNull
    public Completable create(@NonNull Note note) {
        return Completable.fromAction(() -> mNoteDao.create(new NoteDaoModel(note)))
                .subscribeOn(Schedulers.io());
    }

    @NonNull
    public Completable create(@NonNull List<Note> notes) {
        return Completable.fromAction(() -> {
                    List<NoteDaoModel> noteDaoModels = new ArrayList<>(notes.size());

                    for (Note note : notes) {
                        noteDaoModels.add(new NoteDaoModel(note));
                    }

                    mNoteDao.create(noteDaoModels);
                })
                .subscribeOn(Schedulers.io());
    }

    @NonNull
    public Completable delete(@NonNull UUID uuid) {
        return Completable.fromAction(() -> mNoteDao.delete(uuid.toString()))
                .subscribeOn(Schedulers.io());
    }
}
