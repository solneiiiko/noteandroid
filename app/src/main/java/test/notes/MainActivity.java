package test.notes;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.UUID;

import test.notes.ui.edit.EditPagerFragment;
import test.notes.ui.list.ListFragment;

/**
 * Главный экран приложения
 * @author Шумилова О.Н.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

            fragmentTransaction
                    .add(R.id.main_container, ListFragment.newInstance())
                    .commit();
        }
    }

    public void showNewNoteFragment() {
        replaceFragment(EditPagerFragment.newInstance());
    }

    public void showEditNoteFragment(UUID noteId) {
        replaceFragment(EditPagerFragment.newInstance(noteId));
    }

    public void removeFragment() {
        getSupportFragmentManager().popBackStack();
    }

    private void replaceFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_container, fragment)
                .addToBackStack(null)
                .commit();
    }
}
