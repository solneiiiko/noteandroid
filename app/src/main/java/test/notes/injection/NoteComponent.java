package test.notes.injection;

import javax.inject.Singleton;

import dagger.Subcomponent;
import test.notes.datamanager.NotesDataManager;

@Subcomponent(modules = {DataManagerModule.class})
@Singleton
public interface NoteComponent {
    NotesDataManager get();
}
