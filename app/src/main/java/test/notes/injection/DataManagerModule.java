package test.notes.injection;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import test.bases.api.BaseApi;
import test.bases.database.AppDatabase;
import test.bases.injection.AppDatabaseModule;
import test.bases.injection.ContextModule;
import test.notes.api.NotesAPI;
import test.notes.api.RemoteRepository;
import test.notes.dao.LocalRepository;
import test.notes.dao.NoteDao;
import test.notes.datamanager.NotesDataManager;

@Module(includes = {ContextModule.class, AppDatabaseModule.class})
public class DataManagerModule {

    @Provides
    @NonNull
    @Singleton
    NotesDataManager provideDataManager(@NonNull RemoteRepository remoteRepository, @NonNull LocalRepository localRepository) {
        return new NotesDataManager(remoteRepository, localRepository);
    }

    @Provides
    @NonNull
    @Singleton
    RemoteRepository provideRemoteRepository(@NonNull NotesAPI api) {
        return new RemoteRepository(api);
    }

    @Provides
    @NonNull
    @Singleton
    LocalRepository provideLocalRepository(@NonNull NoteDao noteDao) {
        return new LocalRepository(noteDao);
    }

    @Provides
    @NonNull
    @Singleton
    NotesAPI provideApi() {
        return new BaseApi<NotesAPI>().getApi(NotesAPI.class);
    }

    @Provides
    @NonNull
    @Singleton
    NoteDao provideNoteDao(AppDatabase appDatabase) {
        return appDatabase.noteDao();
    }
}
