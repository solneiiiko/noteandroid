package test.bases.ui;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel extends ViewModel {
    @NonNull
    protected final CompositeDisposable mDisposables = new CompositeDisposable();

    @Override
    protected void onCleared() {
        super.onCleared();

        mDisposables.dispose();
    }
}
