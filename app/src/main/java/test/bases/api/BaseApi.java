package test.bases.api;


import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseApi<Api> {
    @NonNull
    private static final String BASE_URL = "http://notes.mrdekk.ru";
    @NonNull
    private static final String TOKEN = "AQAAAAAGp8UAAAURahfth4Tfa0tAqFJZTWF_g5E";

    public Api getApi(@NonNull Class<Api> apiClassName) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHttpClient().build())
                .build()
                .create(apiClassName);
    }

    private OkHttpClient.Builder getHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addInterceptor((chain) -> {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Content-Type", "application/json; charset=utf-8")
                        .header("Authorization", "OAuth" + TOKEN)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            });

        return httpClient;
    }
}
