package test.bases.injection;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import test.bases.database.AppDatabase;

@Module
public class AppDatabaseModule {
    @Provides
    @NonNull
    @Singleton
    AppDatabase provideAppDatabase(@NonNull Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "database-notes")
                .build();
    }
}
