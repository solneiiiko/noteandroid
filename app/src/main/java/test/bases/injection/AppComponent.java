package test.bases.injection;

import dagger.Component;
import test.notes.injection.NoteComponent;

@Component(modules = {ContextModule.class})
public interface AppComponent {
    NoteComponent getNoteComponent();
}
