package test.bases.injection;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    @NonNull
    private Context mContext;

    public ContextModule(@NonNull Context context) {
        mContext = context;
    }

    @Provides
    @NonNull
    @Singleton
    public Context provideContext() {
        return mContext.getApplicationContext();
    }
}
