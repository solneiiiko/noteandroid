package test.bases.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import test.notes.dao.NoteDao;
import test.notes.domain.NoteDaoModel;

@Database(entities = {NoteDaoModel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NoteDao noteDao();
}
